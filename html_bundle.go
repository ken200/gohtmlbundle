package gohtmlbundle

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strings"
)

type HtmlBundleTarget struct {
	CurrentFilePath string
	RootFilePath    string
	Reader          io.Reader
	Remote          bool
}

func Bundle(targets []HtmlBundleTarget) (result []byte, err error) {
	resultSet := NewBundleResultSet(len(targets))
	for i, t := range targets {
		var resultParts []byte
		resultParts, err = bundle(&t)
		if err != nil {
			return
		}
		resultSet.Add(i, resultParts)
	}
	for _, resSetRet := range resultSet.Results() {
		result = append(result, resSetRet...)
	}
	return
}

func bundle(t *HtmlBundleTarget) (result []byte, err error) {
	lineNo := 1
	s := bufio.NewScanner(t.Reader)
	for s.Scan() {
		lineData, lineErr := bundleLine(s.Text(), t.Remote, t.RootFilePath, t.CurrentFilePath)
		if lineErr != nil {
			err = fmt.Errorf("LineNo.%d : %s", lineNo, lineErr.Error())
			return
		}
		result = append(result, lineData...)
		lineNo++
	}
	return
}

func bundleLine(line string, remote bool, rootFilePath string, currentFilePath string) (result []byte, err error) {
	res := BundleResource{}
	exist := extractJSResource(rootFilePath, currentFilePath, line, &res) || extractCSSResource(rootFilePath, currentFilePath, line, &res)
	if !exist {
		result = []byte(line)
		return
	}
	result, err = res.Fetch()
	return
}

func extractJSResource(rootFilePath string, currentFilePath string, str string, js *BundleResource) bool {
	if -1 == strings.Index(str, `<script`) {
		return false
	}
	if -1 == strings.Index(str, `src="`) && -1 == strings.Index(str, `src='`) {
		return false
	}
	resPath := regexp.MustCompile(`src=["'](.*?)["']`)
	submatch := resPath.FindStringSubmatch(str)
	if len(submatch) == 2 {
		js.RootFilePath = rootFilePath
		js.CurrentFilePath = currentFilePath
		js.Location = submatch[1]
		js.ResType = "JavaScript"
	}
	return true
}

func extractCSSResource(rootFilePath string, currentFilePath string, str string, css *BundleResource) bool {
	if -1 == strings.Index(str, `<link`) {
		return false
	}
	if -1 == strings.Index(str, `rel="stylesheet"`) && -1 == strings.Index(str, `rel='stylesheet'`) {
		return false
	}
	if -1 == strings.Index(str, `type="text/css"`) && -1 == strings.Index(str, `type='text/css'`) {
		return false
	}
	if -1 == strings.Index(str, `href="`) && -1 == strings.Index(str, `href='`) {
		return false
	}

	resPath := regexp.MustCompile(`href=["'](.*?)["']`)
	submatch := resPath.FindStringSubmatch(str)
	if len(submatch) == 2 {
		css.RootFilePath = rootFilePath
		css.CurrentFilePath = currentFilePath
		css.Location = submatch[1]
		css.ResType = "CSS"
	}
	return true
}
