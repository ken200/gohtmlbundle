package gohtmlbundle

import (
	"reflect"
	"testing"
)

func TestBundleResultSet(t *testing.T) {
	bRetSet := NewBundleResultSet(3)
	bRetSet.Add(1, []byte("ABC"))
	bRetSet.Add(2, []byte("123"))
	bRetSet.Add(0, []byte("abc"))

	results := bRetSet.Results()

	if len(results) != 3 {
		t.Errorf("[ResultCount] Expand:%d Result:%d", 3, len(results))
		return
	}

	if !reflect.DeepEqual([]byte("abc"), results[0]) {
		t.Errorf("[Result][Index.%d] Expand:%v Result:%v", 0, []byte("abc"), results[0])
		return
	}

	if !reflect.DeepEqual([]byte("ABC"), results[1]) {
		t.Errorf("[Result][Index.%d] Expand:%v Result:%v", 1, []byte("ABC"), results[1])
		return
	}

	if !reflect.DeepEqual([]byte("123"), results[2]) {
		t.Errorf("[Result][Index.%d] Expand:%v Result:%v", 2, []byte("123"), results[2])
		return
	}
}
