package gohtmlbundle

import (
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"
)

func TestBundleJSResourceFetch(t *testing.T) {

	cases := []BundleResource{
		{
			RootFilePath:    "(nil)",
			CurrentFilePath: filepath.Join(os.Getenv("GOPATH"), `src\bitbucket.org\ken200\gohtmlbundle\tests\`),
			Location:        `js/hoge.js`,
			ResType:         `JavaScript`,
		},
		{
			RootFilePath:    filepath.Join(os.Getenv("GOPATH"), `src\bitbucket.org\ken200\gohtmlbundle\tests\`),
			CurrentFilePath: "(nil)",
			Location:        `/js/hoge.js`,
			ResType:         `JavaScript`,
		},
	}

	for i, c := range cases {
		fetchResult, err := c.Fetch()
		if err != nil {
			t.Errorf("[No.%d] Fetch error : %+v", i+1, err)
			return
		}

		expectFetchResult := `<script>(function(){
		function getHoge(){
			return "ほげ";
		}
		(function init(){
			alert(getHoge());
		})();
	})();</script>`

		var convSingleLineString func(src interface{}) []byte
		convSingleLineString = func(src interface{}) []byte {
			if s, ok := src.(string); ok {
				slStr := strings.Replace(s, "\r", "", -1)
				slStr = strings.Replace(slStr, "\n", "", -1)
				slStr = strings.Replace(slStr, "\t", "", -1)
				return []byte(slStr)
			}
			if b, ok := src.([]byte); ok {
				return convSingleLineString(string(b))
			}
			return nil
		}

		if !reflect.DeepEqual(convSingleLineString(fetchResult), convSingleLineString(expectFetchResult)) {
			t.Errorf("[No.%d] expect : %v", i+1, []byte(expectFetchResult))
			t.Errorf("[No.%d] but result : %v", i+1, fetchResult)
		}
	}
}
