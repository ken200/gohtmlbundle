package gohtmlbundle

type BundleResultSet struct {
	results [][]byte
}

func NewBundleResultSet(size int) *BundleResultSet {
	r := &BundleResultSet{}
	r.results = make([][]byte, size)
	return r
}

func (r *BundleResultSet) Add(idx int, data []byte) {
	r.results[idx] = append(r.results[idx], data...)
	//r.results[idx] = data
}

func (r *BundleResultSet) Results() [][]byte {
	return r.results
}
