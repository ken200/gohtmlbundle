package gohtmlbundle

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"reflect"
)

func TestExtractJSResource(t *testing.T) {
	var cases = []struct {
		rootFilePath    string
		currentFilePath string
		scriptStr       string
		expectExist     bool
		expectLocation  string
		expectResType   string
	}{
		{`c:\hoge`, `c:\hoge`, `<html><script src="/js/foo.js"></script></html>`, true, `/js/foo.js`, `JavaScript`},
		{`c:\hoge`, `c:\hoge`, `<html><script src="js/bar.js"></script></html>`, true, `js/bar.js`, `JavaScript`},
		{`c:\hoge`, `c:\hoge`, `<html><script src='/js/foo.js'></script></html>`, true, `/js/foo.js`, `JavaScript`},
		{`c:\hoge`, `c:\hoge`, `<html><script src='js/bar.js'></script></html>`, true, `js/bar.js`, `JavaScript`},
		{`c:\hoge`, `c:\hoge`, `<html><script src='../js/bar.js'></script></html>`, true, `../js/bar.js`, `JavaScript`},
		{`c:\hoge`, `c:\hoge`, `<html><script src='/js/../bar.js'></script></html>`, true, `/js/../bar.js`, `JavaScript`},
	}

	for i, c := range cases {
		js := BundleResource{}
		exist := extractJSResource(c.rootFilePath, c.currentFilePath, c.scriptStr, &js)

		if exist != c.expectExist {
			t.Errorf("No.%d [Exist] Expect:%v | Result:%v", i+1, c.expectExist, exist)
		}
		if exist && js.ResType != c.expectResType {
			t.Errorf("No.%d [ResType] Expect:%v | Result:%v", i+1, c.expectResType, js.ResType)
		}
		if exist && js.Location != c.expectLocation {
			t.Errorf("No.%d [Location] Expect:%v | Result:%v", i+1, c.expectLocation, js.Location)
		}
	}
}

func TestExtractCSSResource(t *testing.T) {
	var cases = []struct {
		rootFilePath    string
		currentFilePath string
		scriptStr       string
		expectExist     bool
		expectLocation  string
		expectResType   string
	}{
		{`c:\hoge`, `c:\hoge`, `<head><link rel="stylesheet" type="text/css" href="/css/hoge.css">`, true, `/css/hoge.css`, `CSS`},
		{`c:\hoge`, `c:\hoge`, `<head><link rel="stylesheet" type="text/css" href="css/hoge.css">`, true, `css/hoge.css`, `CSS`},
		{`c:\hoge`, `c:\hoge`, `<head><link rel="stylesheet" type="text/css" href='/css/hoge.css'>`, true, `/css/hoge.css`, `CSS`},
		{`c:\hoge`, `c:\hoge`, `<head><link rel="stylesheet" type="text/css" href='css/hoge.css'>`, true, `css/hoge.css`, `CSS`},
		{`c:\hoge`, `c:\hoge`, `<head><link rel="stylesheet" type="text/css" href='../css/hoge.css'>`, true, `../css/hoge.css`, `CSS`},
		{`c:\hoge`, `c:\hoge`, `<head><link rel="stylesheet" type="text/css" href='/css/../hoge.css'>`, true, `/css/../hoge.css`, `CSS`},
	}

	for i, c := range cases {
		css := BundleResource{}
		exist := extractCSSResource(c.rootFilePath, c.currentFilePath, c.scriptStr, &css)

		if exist != c.expectExist {
			t.Errorf("No.%d [Exist] Expect:%v | Result:%v", i+1, c.expectExist, exist)
		}
		if exist && css.ResType != c.expectResType {
			t.Errorf("No.%d [ResType] Expect:%v | Result:%v", i+1, c.expectResType, css.ResType)
		}
		if exist && css.Location != c.expectLocation {
			t.Errorf("No.%d [Location] Expect:%v | Result:%v", i+1, c.expectLocation, css.Location)
		}
	}
}

func TestBundle(t *testing.T) {
	target := HtmlBundleTarget{
		RootFilePath: filepath.Join(os.Getenv("GOPATH"), `src\bitbucket.org\ken200\gohtmlbundle`),
		Reader: bytes.NewBuffer([]byte(`<html>
	<head>
		<link rel="stylesheet" type="text/css" href="/tests/css/bar.css"></link>
		<script src="/tests/js/foo.js"></script>
	</head>
	<body>
		<h1>見出し見出し見出し見出し見出し見出し見出し見出し</h1>
	</body>
<script src="/tests/js/hoge.js"></script>
</html>`)),
		Remote: false,
	}

	result, err := Bundle([]HtmlBundleTarget{target})
	if err != nil {
		t.Errorf("occurred a error : %+v", err)
		return
	}

	expandResult := []byte(`<html>
	<head>
		<style>.bar {
	background: #ff8040;
	margin: 0;
	padding: 0;
}

.barbar {
	float: left;
}</style>
		<script>function getFoo(){
	return "ふー";
}

function alertFoo(){
	alert(getFoo());
}</script>
	</head>
	<body>
		<h1>見出し見出し見出し見出し見出し見出し見出し見出し</h1>
	</body>
<script>(function(){
	function getHoge(){
		return "ほげ";
	}
	(function init(){
		alert(getHoge());
	})();
})();</script>
</html>`)

	var convSingleLineString func(src interface{}) []byte
	convSingleLineString = func(src interface{}) []byte {
		if s, ok := src.(string); ok {
			slStr := strings.Replace(s, "\r", "", -1)
			slStr = strings.Replace(slStr, "\n", "", -1)
			slStr = strings.Replace(slStr, "\t", "", -1)
			slStr = strings.Replace(slStr, " ", "", -1)
			return []byte(slStr)
		}
		if b, ok := src.([]byte); ok {
			return convSingleLineString(string(b))
		}
		return nil
	}

	if !reflect.DeepEqual(convSingleLineString(expandResult), convSingleLineString(result)) {
		t.Errorf("result not matched")
		t.Errorf("expand: %v", convSingleLineString(expandResult))
		t.Errorf("result: %v", convSingleLineString(result))
	}
}
