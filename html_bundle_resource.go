package gohtmlbundle

import "strings"
import "errors"
import "path/filepath"

import "io/ioutil"
import "os"

type BundleResource struct {
	CurrentFilePath string
	RootFilePath    string
	Location        string //リソースの場所(ローカルorリモート)がFetch()するまで不明なためLocationとしている
	ResType         string
}

func (b *BundleResource) isAbsoluteLocalResource() bool {
	return !b.isRemoteHttpResource() && strings.HasPrefix(b.Location, `/`)
}

func (b *BundleResource) isRelativeLocalResource() bool {
	return !b.isRemoteHttpResource() && !strings.HasPrefix(b.Location, `/`)
}

func (b *BundleResource) isRemoteHttpResource() bool {
	return strings.HasPrefix(b.Location, `http://`) || strings.HasPrefix(b.Location, `https://`)
}

func defaultPathIfEmpty(path string) (ret string) {
	if path == "" {
		ret, _ = os.Getwd()
	} else {
		ret = path
	}
	return
}

func (b *BundleResource) Fetch() (ret []byte, err error) {
	defer func() {
		switch strings.ToLower(b.ResType) {
		case "javascript", "js":
			ret = append([]byte(`<script>`), ret...)
			ret = append(ret, []byte(`</script>`)...)
		case "css":
			ret = append([]byte(`<style>`), ret...)
			ret = append(ret, []byte(`</style>`)...)
		}
	}()

	if b.isAbsoluteLocalResource() {
		resFilePath := strings.Replace(b.Location, `/`, string(filepath.Separator), -1)
		resFilePath = filepath.Join(defaultPathIfEmpty(b.RootFilePath), resFilePath)
		ret, err = ioutil.ReadFile(resFilePath)
	} else if b.isRelativeLocalResource() {
		resFilePath := strings.Replace(b.Location, `/`, string(filepath.Separator), -1)
		resFilePath = filepath.Join(defaultPathIfEmpty(b.CurrentFilePath), resFilePath)
		ret, err = ioutil.ReadFile(resFilePath)
	} else if b.isRemoteHttpResource() {
		//todo:httpリクエスト
	} else {
		err = errors.New("invalid resource location : " + b.Location)
	}
	return
}
